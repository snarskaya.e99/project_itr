import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.sass']
})
export class CardsComponent {
  @Input() news:{ source: string; articles: string; author: string; title: string; description: string; content: string;}[] = [];
}
