import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.sass']
})
export class TestComponent implements OnInit {

 news = [
   {source: "", articles: "", author: "", title: "", description: "", content: ""}
 ];

  condition: boolean=true;

  constructor(private http: HttpClient){}

  ngOnInit(){
    this.http.get('https://newsapi.org/v2/top-headlines?country=us&apiKey=d2452c6884a44a98b5eb7c784c8ce69e').subscribe((data:any) => {this.news =  data.articles; console.log(this.news)});
  }


  switchView(){
    this.condition=!this.condition;
  }

}
